package homework2;

/*
Написать программу "стрельба по площади".
Технические требования:
Дан квадрат 5х5, где программа случайным образом ставит цель.
Перед началом игры на экран выводится текст: All set. Get ready to rumble!
Сам процесс игры обрабатывается в бесконечном цикле.
Игроку предлагается ввести линию для стрельбы.
Программа проверяет что бьло введено число, и введенная линия находится в границах игрового поля (1-5).
В случае, если игрок ошибся предлагает ввести число еще раз.
Игроку предлагается ввести столбик для стрельбы (должен проходить аналогичную проверку).
После каждого выстрела необходимо отображать обновленное игровое поле в консоли. Клетки, куда игрок уже стрелял, необходимо отметить как *.
Игра заканчивается при поражении цели. В конце игры вывести в консоль фразу You have won!, а также игровое поле. Пораженную цель отметить как x.
Задание должно быть выполнено ипспользуя массивы (НЕ используйте интерфейсы List, Set, Map).
Пример вывода в консоль:

0 | 1 | 2 | 3 | 4 | 5 |
1 | - | - | - | - | - |
2 | - | * | * | - | - |
3 | * | - | - | * | - |
4 | - | - | - | - | * |
5 | * | - | * | - | - |
*/

import java.util.Random;
import java.util.Scanner;

public class ShootingSquare {

    public static Random random = new Random();
    public static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println();
        System.out.println("-=All set. Get ready to rumble!=-");

        String[][] playField = {
                {"0 |", " 1 |", " 2 |", " 3 |", " 4 |", " 5 |"},
                {"1 |", " - |", " - |", " - |", " - |", " - |"},
                {"2 |", " - |", " - |", " - |", " - |", " - |"},
                {"3 |", " - |", " - |", " - |", " - |", " - |"},
                {"4 |", " - |", " - |", " - |", " - |", " - |"},
                {"5 |", " - |", " - |", " - |", " - |", " - |"},
        };

        int verCord;
        int horCord;
        boolean verFlag;
        boolean horFlag;
        int verRandom = random.nextInt(5) + 1;
        int horRandom = random.nextInt(5) + 1;

        do {

            verCord = 0;
            horCord = 0;
            verFlag = false;
            horFlag = false;

            System.out.println();
            System.out.println("Please input the vertical coordinate.");
            System.out.print("(a whole number from 1 to 5 inclusive): ");
            while (!verFlag) {
                if (!console.hasNextInt()) {
                    console.reset();
                    console = new Scanner(System.in);
                } else {
                    verCord = console.nextInt();
                }
                if ((verCord < 1 || verCord > 5)) {
                    System.out.print("Invalid value, please try again: ");
                } else {
                    verFlag = true;
                }
            }

            System.out.println();
            System.out.println("Please input the horizontal coordinate.");
            System.out.print("(a whole number from 1 to 5 inclusive): ");
            while (!horFlag) {
                if (!console.hasNextInt()) {
                    console.reset();
                    console = new Scanner(System.in);
                } else {
                    horCord = console.nextInt();
                }
                if ((horCord < 1 || horCord > 5)) {
                    System.out.print("Invalid value, please try again: ");
                } else {
                    horFlag = true;
                }
            }

            if (verRandom != verCord || horRandom != horCord) {
                playField[verCord][horCord] = " * |";
            } else {
                playField[verCord][horCord] = " X |";
                System.out.println();
                System.out.println("-=You have won!=-");
            }

            System.out.println();
            for (int ver = 0; ver < playField.length; ver++) {
                for (int hor = 0; hor < playField.length; hor++) {
                    System.out.print(playField[ver][hor]);
                }
                System.out.println();
            }

        } while (verRandom != verCord || horRandom != horCord);
        System.out.println();
    }
}