package homework1;

/*
Написать программу "числа", которая загадывает случайное число и предлагает игроку его угадать.
Технические требования:
С помощью java.util.Random программа загадывает случайное число в диапазоне [0-100] и предлагает игроку через консоль ввести свое имя, которое сохраняется в переменной name.
Перед началом на экран выводится текст: Let the game begin!
Сам процесс игры обрабатывается в бесконечном цикле.
Игроку предлагается ввести число в консоль, после чего программа сравнивает загаданное число с тем, что ввел пользователь.
Если введенное число меньше загаданного, то программа выводит на экран текст: Your number is too small. Please, try again.
Если введенное число больше загаданного, то программа выводит на экран текст: Your number is too big. Please, try again.
Если введенное число соответствуют загаданному, то программа выводит текст: Congratulations, {name}!
Задание должно быть выполнено ипспользуя массивы (НЕ используйте интерфейсы List, Set, Map).
*/

import java.util.Random;
import java.util.Scanner;

public class Numbers {
    public static void main(String[] args) {

        Random random = new Random();
        int randomNum = random.nextInt(101);
        Scanner console = new Scanner(System.in);
        System.out.print("Please enter your name: ");
        String name = console.nextLine();
        System.out.println("Let the game begin!");

        int userNum;
        do {
            System.out.print("Please enter your number: ");
            userNum = console.nextInt();
            if (userNum > randomNum) {
                System.out.println("Your number is too big. Please, try again.");
            } else if (userNum < randomNum) {
                System.out.println("Your number is too small. Please, try again.");
            } else {
                System.out.println("Congratulations, {" + name + "}!");
            }
        } while (userNum != randomNum);
    }
}