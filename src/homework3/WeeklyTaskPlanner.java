package homework3;

/*
Написать консольную программу "планировщик задач на неделю".
Технические требования:
Создать двумерный массив строк размерностью 7х2 String[][] schedule = new String[7][2]
Заполните матрицу значениями день недели: главное задание на данный день:
schedule[0][0] = "Sunday";
schedule[0][1] = "do home work";
schedule[1][0] = "Monday";
schedule[1][1] = "go to courses; watch a film";
Используя цикл и оператор switch, запросите у пользователя ввести день недели в консоль и, в зависимости от ввода:
программа: Please, input the day of the week:
    пользователь вводит корректный день недели (f.e. Monday)
    программа выводит на экран Your tasks for Monday: go to courses; watch a film.
    программа идет на следующую итерацию
программа: Please, input the day of the week:
    пользователь вводит некорректный день недели (f.e. %$=+!11=4)
    программа выводит на экран Sorry, I don't understand you, please try again.
    программа идет на следующую итерацию до успешного ввода
программа: Please, input the day of the week:
    пользователь выводит команду выхода exit
    программа выходит из цикла и корректно завершает работу
Задание должно быть выполнено используя массивы (НЕ используйте интерфейсы List, Set, Map).
Учтите: программа должна принимать команды как в нижнем регистре (monday) так и в верхнем (MONDAY) и учитывать, что пользователь мог случайно после дня недели ввести пробел.
*/

import java.util.Objects;
import java.util.Scanner;

public class WeeklyTaskPlanner {

    public static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {

        String[][] schedule = new String[7][2];
        schedule[0][0] = "Monday";
        schedule[0][1] = "work; do homework.";
        schedule[1][0] = "Tuesday";
        schedule[1][1] = "work; online lecture.";
        schedule[2][0] = "Wednesday";
        schedule[2][1] = "work; do homework.";
        schedule[3][0] = "Thursday";
        schedule[3][1] = "work; online lecture.";
        schedule[4][0] = "Friday";
        schedule[4][1] = "work; do homework.";
        schedule[5][0] = "Saturday";
        schedule[3][1] = "online lecture; work.";
        schedule[6][0] = "Sunday";
        schedule[6][1] = "do homework; work.";

        String day = null;

        while (!Objects.equals(day, "exit")) {
            System.out.println();
            System.out.print("Please, input the day of the week: ");
            day = console.nextLine();
            day = day.toLowerCase().trim();
            switch (day) {
                case "monday" -> System.out.println("Your tasks for " + schedule[0][0] + ": " + schedule[0][1]);
                case "tuesday" -> System.out.println("Your tasks for " + schedule[1][0] + ": " + schedule[1][1]);
                case "wednesday" -> System.out.println("Your tasks for " + schedule[2][0] + ": " + schedule[2][1]);
                case "thursday" -> System.out.println("Your tasks for " + schedule[3][0] + ": " + schedule[3][1]);
                case "friday" -> System.out.println("Your tasks for " + schedule[4][0] + ": " + schedule[4][1]);
                case "saturday" -> System.out.println("Your tasks for " + schedule[5][0] + ": " + schedule[5][1]);
                case "sunday" -> System.out.println("Your tasks for " + schedule[6][0] + ": " + schedule[6][1]);
                case "exit" -> System.exit(0);
                default -> System.out.println("Sorry, I don't understand you, please try again.");
            }
        }
    }
}